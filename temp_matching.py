# -*- coding: utf-8 -*-
"""
Created on Thu May 17 14:00:00 2018

@author: SHREEKANTH
"""

import cv2
import math
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread(r'C:\Users\SHREEKANTH\Desktop\ree.jpg')
ret,img1 = cv2.threshold(img,150,255,cv2.THRESH_BINARY)
dim = img.shape
for i in range(50):
    if img1[i,5].all() == 0:
        break;

print(i)        
img = img [i:278,:800]
print(img.shape)
#print(dim)
#dim = (1654,278)
#img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
cv2.imwrite('re.jpg',img)
#ix = img[int(y):int(y1),int(x):int(x1)]
#cv2.imshow('A',ix)
#img = cv2.bitwise_not(img)
img2 = img.copy()
template = cv2.imread(r'C:\Users\SHREEKANTH\Desktop\template.jpg')
#template = cv2.bitwise_not(template)
w=template.shape[0]
h=template.shape[1]

# All the 6 methods for comparison in a list
#methods = ['cv2.TM_CCOEFF','cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
#           'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']


img = img2.copy()
    

    # Apply template Matching
res = cv2.matchTemplate(img,template,cv2.TM_SQDIFF)
min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

# If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
top_left = min_loc
bottom_right = (top_left[0] + w, top_left[1] + h)
print(top_left,bottom_right)
#cv2.rectangle(img,top_left, bottom_right, 255, 2)

c1=20-top_left[0]
c2=127-top_left[1]

add = cv2.imread(r'C:\Users\SHREEKANTH\Desktop\add_on.jpg')
ver = add[0:img.shape[0],0:c1]
vis = np.concatenate((ver,img), axis=1)
hori = add[0:c2,0:vis.shape[1]]
vis = np.concatenate((hori,vis), axis=0)
print(ver.shape,hori.shape)
cv2.imshow('vis',vis)
cv2.waitKey(0)
cv2.imwrite('out.jpg', vis)

t1=209-127  
t2=610-20   #box's top_left coordinate
t3=248-127
t4=649-20   #box's bottom_right coordinate

angle = np.arctan2(t1, t2)
z = math.sqrt(t1**2+t2**2)
x = 20 + z*np.cos(angle)
y = 127 + z*np.sin(angle)

angle1 = np.arctan2(t3, t4)
z1 = math.sqrt(t3**2+t4**2)
x1 = 20 + z1*np.cos(angle1)
y1 = 127 + z1*np.sin(angle1)

print(x,y,angle)
print(x1,y1,angle1)
print(int(x),int(x1),int(y),int(y1))

img1 = vis[int(y):int(y1),int(x):int(x1)]
#cv2.imshow('A',img1)

ret,img1 = cv2.threshold(img1,245,255,cv2.THRESH_BINARY_INV)
cv2.imshow('A',img1)

dim = (28,28)

res = cv2.resize(img1, dim, interpolation = cv2.INTER_AREA)
print(res.shape)

res1= res.copy()

res1 = cv2.cvtColor(res1,cv2.COLOR_BGR2GRAY)
cv2.imshow('res1',res1)

cv2.waitKey(0)

from keras.models import load_model
model=load_model('ocr.model')
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

x1=np.array(res1)
x1=x1.reshape(1,28,28,1)
classes = model.predict_classes(x1)
print(classes)

